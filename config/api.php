<?php

return [
    'basketball' => [
        'url' => 'https://v1.basketball.api-sports.io/',
        'key' => env('BASKETBALL_API_KEY')
    ]
];

