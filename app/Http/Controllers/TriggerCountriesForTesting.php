<?php


namespace App\Http\Controllers;

use App\Models\Country;
use App\Services\ApiClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Season;
class TriggerCountriesForTesting
{
    private $logger;

    public function startCountries(ApiClient $apiClient,Country $country) : void
    {
        try {
            with(new \App\Jobs\ApiBasketball\CountriesCoordinator($apiClient,$country))->handle();
        } catch (\RuntimeException $e) {
            log::info('can\'t run insert countries');
            return;

        };
    }
}
