<?php

namespace App\Http\Controllers;

use App\Services\ApiClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Season;
class TriggerSeasonsForTesting extends Controller
{
    private $logger;

    public function startSeason(ApiClient $apiClient,Season $season) : void
    {
        try {
        with(new \App\Jobs\ApiBasketball\SeasonsCoordinator($apiClient,$season))->handle();
        } catch (\RuntimeException $e) {
            log::info('cant run insert seasons');
            return;
        };
    }
}
