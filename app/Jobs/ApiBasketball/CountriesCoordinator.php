<?php


namespace App\Jobs\ApiBasketball;

use App\Services\ApiClient;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use App\Models\Country;
class CountriesCoordinator
{
    private $logger;

    public function __construct(
        ApiClient $apiClient,
        Country $country
    )
    {
        $this->apiClient = $apiClient;
        $this->country = $country;
    }

    public function handle()
    {
        try {
            $response = $this->apiClient->sendRequest('countries', 'response');
        } catch (\RuntimeException $e) {
            log::info('Countries handle error:Unable to send api request');
            return;
        }


        $this->country->createFromCollection($response);

        Log::info("Countries handle is completed");
    }
}

