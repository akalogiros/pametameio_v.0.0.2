<?php

namespace App\Jobs\ApiBasketball;

use App\Services\ApiClient;
use Illuminate\Support\Facades\Log;
use App\Models\Season;

class SeasonsCoordinator
{
    private $apiClient;
    private $season;

    public function __construct(
        ApiClient $apiClient,
        Season $season
    )
    {
        $this->apiClient = $apiClient;
        $this->season = $season;
    }

    public function handle()
    {
        try {
            $response = $this->apiClient->sendRequest('seasons', 'response');
        } catch (\RuntimeException $e) {
            log::info('Countries handle error:Unable to send api request');
            return;
        }
        //$removedElement = array_pop($response);
        $output = [];
        foreach ($response as $key => $value) {
            if (is_string($value)) {
                $output[] = [
                    'period' => $value,
                    'filter' => $response[$key + 1]
                ];
            }
        }

        $this->season->createFromCollection($output);

        Log::info("Seasons handle is completed");

    }
}
