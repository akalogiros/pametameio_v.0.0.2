<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class ApiClient
{
    public function sendRequest($apiParameter, $arrayAccessField)
    {
        return Http::withHeaders(['x-rapidapi-key' => env('BASKETBALLL_API_KEY')])
            ->get('https://v1.basketball.api-sports.io/'.$apiParameter)[$arrayAccessField];
    }
}

