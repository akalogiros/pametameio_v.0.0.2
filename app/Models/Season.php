<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Season extends Model
{
    protected $fillable = ['id','period','filter'];
    public $timestamps = false;

    public function createFromCollection($collection) : void    {
        foreach ($collection as $season) {
            try {
                self::firstOrCreate($season);
            } catch (\RuntimeException $e) {
                $exception = $e->getMessage();
                log::info($exception);
                return;
            }
        }
    }
}
