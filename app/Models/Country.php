<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Country extends Model
{
    protected $fillable = ['id', 'name', 'code'];
    public $timestamps = false;

    public function createFromCollection($collection): void
    {
        foreach ($collection as $country) {
            try {
                unset($country['flag']);
                self::firstOrCreate($country);
            } catch (\RuntimeException $e) {
                log::info('cannot insert seasons to database ');
                return;
            }
        }
    }
}
