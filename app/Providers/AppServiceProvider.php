<?php

namespace App\Providers;

use App\Jobs\ApiBasketball\ApiClient;
use App\Jobs\ApiBasketball\Seasons\SeasonFactory;
use App\Jobs\ApiBasketball\Seasons\SeasonResponseValidator;
use App\Jobs\ApiBasketball\Seasons\Seasons;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
