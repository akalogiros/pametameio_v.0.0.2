<?php



namespace App\Jobs\ApiBasketball;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
|
*/


//Route::get('/seasons', '\App\Jobs\ApiBasketball\Seasons\SeasonsCoordinator@handle');
Route::get('/fetchseasons', 'TriggerSeasonsForTesting@startSeason'); //ok
Route::get('/fetchcountries', 'TriggerCountriesForTesting@startCountries');  //not ok

Route::get('/', function () {
    return view('welcome');
});
